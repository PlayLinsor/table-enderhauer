<?php
require_once 'include/foot.php';
require_once 'include/guifunction.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Электронная матрица Эйзенхауэра</title>
<link href="images/styles.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table border="0" id="bodytable" align="center" >
  <tr>
    <td width="203" id="topmenublock">&nbsp;</td>
    <td width="103" class="contentcolor topcenterblock">&nbsp;</td>
    <td width="465" class="contentcolor topcontentblock"><h1>Экономь время</h1></td>
  </tr>
  <tr>
    <td rowspan="2" style="background-color: #C8C6C5;" height="250px" id="logomerge1"><a href="index.php"><img src="images/logo1.jpg" width="195px" height="250" alt="Часть блокнота "/></a></td>
    <td rowspan="2" class="contentcolor" id="logomerge2"><a href="index.php"><img src="images/logo2.jpg" alt="Блокнот с матрицей" height="188"/></a></td>
    <td class="contentcolor sloganblock">С помощью электронной матрицы Эйзенхауэра</td>
  </tr>
  <tr>
    <td class="contentcolor top">
    <?php include $_SESSION['modul1']; ?>
	</td>
  </tr>
  <tr>
    <td class="menufon top">
	<div id="menudiv">
	<ul>
	  <li <?php PrintClassStatusMenu(1)?> >
	   <a href="index.php" <?php PrintClassStatusMenu(1)?> >Главная</a></li>
	  <li <?php PrintClassStatusMenu(2)?> >
	   <a href="index.php?page=profile" <?php PrintClassStatusMenu(2)?> >Профиль</a></li>
	  <li <?php PrintClassStatusMenu(3)?> >
	   <a href="index.php?page=readmore" <?php PrintClassStatusMenu(3)?> >Подробнее</a></li>
	  <li <?php PrintClassStatusMenu(4)?> >
	   <a href="index.php?page=mail" <?php PrintClassStatusMenu(4)?>>Обратная связь</a></li>
	 </ul>
	</div>	</td>
    <td colspan="2" class="contentcolor top shadow">
    <?php include $_SESSION['modul2']; ?>
    </td>
  </tr>
  <tr>
    <td colspan="3" id="copiright">Create by PlayLinsor@gmail.com </td>
  </tr>
  <tr>
    <td class="menufon">&nbsp;</td>
    <td class="contentcolor">&nbsp;</td>
    <td class="contentcolor">&nbsp;</td>
  </tr>
</table>

</body>
</html>
